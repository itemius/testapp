//
//  ViewController.swift
//  testapp
//
//  Created by Artem Izyumov on 14/03/2019.
//  Copyright © 2019 Artem Izyumov. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterTextField: UITextField!
    private let refreshControl = UIRefreshControl()
    var array: [ZipModel] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZipCell", for: indexPath) as! TestTableViewCell
        cell.sizeLabel.text = array[indexPath.row].size.description
        cell.versionLabel.text = array[indexPath.row].version.description
        return cell

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc private func refreshData(_ sender: Any) {
        
        defer {
            self.refreshControl.endRefreshing()
        }
        self.array.removeAll()

        if let version = Int(filterTextField.text!) {
            APIClient.getList(version: version.description) { response in
                switch response.result {
                    
                case .success(_):
                    if((response.result.value) != nil) {
                        let json = JSON(response.result.value!)
                        if let zips = json["ziplist"].array {
                            for zip in zips {
                                var zipModel = ZipModel()
                                if let version = zip["version"].int {
                                    zipModel.version = version
                                }
                                if let size = zip["size"].int {
                                    zipModel.size = size
                                }
                                if let url = zip["url"].string {
                                    zipModel.url = url
                                }
                                self.array.append(zipModel)
                            }
                        }
                        self.tableView.reloadData()
                    }
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: "ERROR: \(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            self.tableView.reloadData()
        }
    }
    
}
