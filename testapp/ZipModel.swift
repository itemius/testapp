//
//  ZipModel.swift
//  testapp
//
//  Created by Artem Izyumov on 14/03/2019.
//  Copyright © 2019 Artem Izyumov. All rights reserved.
//

struct ZipModel {
    var url: String = ""
    var size: Int = 0
    var version: Int = 0

}
