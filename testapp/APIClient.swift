//
//  APIClient.swift
//  testapp
//
//  Created by Artem Izyumov on 14/03/2019.
//  Copyright © 2019 Artem Izyumov. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

class APIClient {
    
    static let endpoint = "http://vetmag.xclm.ru/data/ziplist/distr3/"
    
    static let manager = SessionManager(configuration: APIClient.configuration())
    
    static func configuration() -> URLSessionConfiguration {
        let configuration = SessionManager.default.session.configuration
        
        configuration.timeoutIntervalForRequest = 20
        configuration.httpAdditionalHeaders = ["Accept": "application/json"]
        
        return configuration
    }
    
    static func getList(completion: @escaping (DataResponse<Any>) -> Void) {
        manager.request(self.endpoint + "0/", method: .get).responseJSON(completionHandler: completion)
    }
    
    static func getList(version: String, completion: @escaping (DataResponse<Any>) -> Void) {
        manager.request(self.endpoint + "\(version)/", method: .get).responseJSON(completionHandler: completion)
    }
    
}
